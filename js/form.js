/* ------------------------------------------- DOM READY -------------------------------------------------------- */
/* Перерисовать */
jQuery.fn.redraw = function () {
    return this.hide(0, function () {
        $(this).show();
    });
};

$(document).ready(function () {
    
	/* Открыть модальное окно */
    $(".call-form").click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#semitransparentoverlay2').redraw();
        $('.contact-modalmsg, #semitransparentoverlay2').fadeIn('200');
		$('.contact-modalmsg-title').html($(this).data('title')); 	// назначение титла окна
		$('#modal-form-subject').val($(this).data('subject'));		// назначение темы письма
    });
	
	/* Обработка отправки формы */
	$(".submit-form").click(function(){
		var err = '';
		var form_id = $(this).parent().attr('id');
		var name = $("#"+form_id+" .form-name").val();
		var email = $("#"+form_id+" .form-email").val();
		if ($("#"+form_id+" .form-phone").length) {
			var phone = $("#"+form_id+" .form-phone").val();
		} else
			var phone = -1;
	
		/*if (name == '') {
			err = err + 'Пожалуйста, введите имя';
		}
		*/
		
		if (err == '' && email == '') {
			if (phone == -1)
			err = err + '<BR>Пожалуйста, введите email или телефон';
			else
			err = err + '<BR>Пожалуйста, введите email и телефон';
		}
		
		if (err == '' && phone == '') {
			err = err + '<BR>Пожалуйста, введите телефон';
		}
		
		if (err != '') showMessage(form_id,err,'form-error'); else {
			$.ajax({
			  type: "POST",
			  url: "/form.php",
			  data: $("#"+form_id).serialize(),
			  success: function() {showMessage(form_id,'Спасибо! Ваша заявка отправлена. Мы перезвоним.','form-success');}			 
			});
			
		}
		
	});
	
	function showMessage(form_id,m,c) {
		$("#"+form_id+" .form-message").show();
		$("#"+form_id+" .form-message").addClass(c);
		$("#"+form_id+" .form-message").html(m);
		setTimeout(function(){$("#"+form_id+" .form-message").hide(1500);},500);
		$("#"+form_id+" .form-message").removeClass(c);
	}

	/* Закрыть модальное окно */
    $('.contact-modalmsg .btn-close').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).closest('.contact-modalmsg').fadeOut('200');
        $('#semitransparentoverlay2').fadeOut('200');
    });

});