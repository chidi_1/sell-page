jQuery.fn.redraw = function () {
    return this.hide(0, function () {
        $(this).show();
    });
};

// Функция предзагрузки изображений
function PreLoadImages(urls){
	var imgs = Array();
	for (var i = 0; i < urls.length; i++) {
		imgs[i] = new Image();
		imgs[i].src = urls[i];
	}
}

/* ---------------------------------- DOM READY ----------------------------------- */
$(document).ready(function () {
    'use strict';

	// Предзагрузка изображений
	PreLoadImages(['../img/1bann.jpg', '../img/2bann.jpg']);
	
    // Меню
    $('.header .nav li a').click(function (e) {
        e.preventDefault();
        var elid = $(this).attr('href');
        var toscr = $(elid).offset().top - 95 + 'px';
        $('html, body').animate({
            'scrollTop': toscr
        }, 300);
    });
	
	// Слайдер
	$("#nivoSlider").nivoSlider({
        effect:"boxRainGrowReverse",
        slices:15,
        boxCols:1,
        boxRows:1,
        animSpeed:500,
        pauseTime:10000,
        startSlide:0,
        directionNav:true,
		prevText: '',       
        nextText: '', 
        controlNav:true,
        controlNavThumbs:false,
        pauseOnHover:false,
        manualAdvance:false
    });

    // Модальное окно
    $('.services-list li').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var target_id = $(this).data('target');
        $('#semitransparentoverlay').redraw();
        $('#' + target_id + ', #semitransparentoverlay').fadeIn('200');
    });

    $('.modalmsg .btn-close').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).closest('.modalmsg').fadeOut('200');
        $('#semitransparentoverlay').fadeOut('200');
    });

    // Кейсы
    $(".cases-carousel").jCarouselLite({
        auto: 0,
        speed: 1000,
        circular: true,
        scroll: 3,
        btnNext: ".cases-carousel-next",
        btnPrev: ".cases-carousel-prev"
    });
	
	// Лидеры регионов
    $(".leaders-carousel").jCarouselLite({
        auto: 0,
        speed: 1000,
        circular: true,
        scroll: 1,
        btnNext: ".leaders-carousel-next",
        btnPrev: ".leaders-carousel-prev"
    });

    // Интернет-магазины
    $(".online-shops-carousel").jCarouselLite({
        auto: 0,
        speed: 1000,
        circular: true,
        scroll: 4,
        visible: 4,
        btnNext: ".online-shops-carousel-next",
        btnPrev: ".online-shops-carousel-prev"
    });

    // Карта
    ymaps.ready(init);

    function init() {
        var geolocation = ymaps.geolocation,
            coords = [geolocation.latitude, geolocation.longitude],
            myMap = new ymaps.Map('map', {
                center: coords,
                zoom: 14
            });
    }

    // попап с портфолио
    $('.fancy').fancybox({
        afterLoad:function(){
            $('.fancybox-wrap').append('<div class="btn-wrap"><input name="" type="button" value="Хотите такой же?" class="btn btn-1 js--request" data-title="Бесплатная консультация" data-subject="Заявка на консультацию"></div>>')
        }
    });

    $(document).on('click', '.js--show-more', function(){
        var content = $(this).parents('.cases').find('.second-start').html();
        $(this).parents('.cases').find('.first-start').empty().append(content);
        $(this).parents('.button-wrap').addClass('hidden-block');

        var support = { transitions: Modernizr.csstransitions },
				// transition end event name
				transEndEventNames = { 'WebkitTransition': 'webkitTransitionEnd', 'MozTransition': 'transitionend', 'OTransition': 'oTransitionEnd', 'msTransition': 'MSTransitionEnd', 'transition': 'transitionend' },
				transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
				onEndTransition = function( el, callback ) {
					var onEndCallbackFn = function( ev ) {
						if( support.transitions ) {
							if( ev.target != this ) return;
							this.removeEventListener( transEndEventName, onEndCallbackFn );
						}
						if( callback && typeof callback === 'function' ) { callback.call(this); }
					};
					if( support.transitions ) {
						el.addEventListener( transEndEventName, onEndCallbackFn );
					}
					else {
						onEndCallbackFn();
					}
				};

			new GridFx(document.querySelector('.grid.first-start'), {
				imgPosition : {
					x : -0.5,
					y : 1
				},
				onOpenItem : function(instance, item) {
					instance.items.forEach(function(el) {
						if(item != el) {
							var delay = Math.floor(Math.random() * 50);
							el.style.WebkitTransition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), -webkit-transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
							el.style.transition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
							el.style.WebkitTransform = 'scale3d(0.1,0.1,1)';
							el.style.transform = 'scale3d(0.1,0.1,1)';
							el.style.opacity = 0;
						}
					});
				},
				onCloseItem : function(instance, item) {
					instance.items.forEach(function(el) {
						if(item != el) {
							el.style.WebkitTransition = 'opacity .4s, -webkit-transform .4s';
							el.style.transition = 'opacity .4s, transform .4s';
							el.style.WebkitTransform = 'scale3d(1,1,1)';
							el.style.transform = 'scale3d(1,1,1)';
							el.style.opacity = 1;

							onEndTransition(el, function() {
								el.style.transition = 'none';
								el.style.WebkitTransform = 'none';
							});
						}
					});
				}
			});
        return false;
    })

    $(document).on('click', '.js--request', function(){
        $('.fancybox-close').trigger('click');
        $('.footer.block-1 .wrapper .btn').trigger('click');
    })
});